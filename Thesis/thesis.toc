\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{DEDICATION}{iv}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{ACKNOWLEDGMENTS}{v}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{LIST OF FIGURES}{ix}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}INTRODUCTION}{1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Communication}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Satellite Communications}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{GEO Satellites}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Multirotor Unmanned Aerial Vehicles}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Stability and Efficiency}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Efficiency}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Conclusions}{10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Contributions}{10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}LITERATURE REVIEW}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Current Solutions}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Portable Satellite Backhauling Solution}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Emergency Communication System Based on IP and Airship}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Feasibility}{17}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}\uppercase {Proposed UAV Communication System}}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Hardware}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Autopilot}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Telemetry}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}GPS}{24}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Software}{25}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Python Application}{25}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Finite-State Machine Class}{30}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Clustering}{32}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}User Interface}{35}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{HTML5 WebSockets}{38}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}System Operation}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Launch Sequence}{40}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Fail-safes}{42}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}RESULTS}{43}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}System Performance}{43}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}User Interface}{44}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Simulator}{45}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Remote Access}{46}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Hardware Performance}{49}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.1}Telemetry}{49}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.2}GPS}{50}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}The State of UAVs in the United States and the FAA}{50}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}CONCLUSION}{52}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}FUTURE WORK}{52}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{BIBLIOGRAPHY}{54}
